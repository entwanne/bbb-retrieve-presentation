# Scrap Big Blue Button video

Script to retrieve presentation from BBB records.

## Install python dependencies

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

```
./bin/scrap_video.py retrieve <meeting_video_url>
```

Then get your BBB replay at `<yourserver>/playback.html?meetingId=<your_meeting_id>`.

*Attention peinture fraîche*

Except librairies, CSS and html files used in public and are from [Big Blue Button](https://github.com/bigbluebutton), this project is in

<a href="http://www.wtfpl.net/"><img
    src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png"
    width="80" height="15" alt="WTFPL" /></a>
